describe('EditAdvertComponent', () => {
    beforeEach(() => {
        cy.visit('/edit-advert/1');
  
      // Mock the HTTP request made by the component to fetch the advert details for editing
      cy.intercept('GET', '/advert/*', {
        statusCode: 200,
        body: {
          idAdvert: 1,
          title: 'Original Title',
          description: 'Original description.',
          price: '100',
          picture: 'original-picture-path',
          idClient: 1
        }
      }).as('fetchAdvert');
  
      // Mock the HTTP request made to update the advert
      cy.intercept('PUT', '/advert', {
        statusCode: 200,
        body: {
            idAdvert: 1,
            title: 'Original Title',
            description: 'Original description.',
            price: '100',
            picture: 'original-picture-path',
            idClient: 1
        }
      }).as('updateAdvert');
    });
  
    it('successfully edits an advert', () => {
      // Wait for the advert details to be fetched
      cy.wait('@fetchAdvert');
  
      // Update the form fields with new values
      cy.get('input[name="title"]').clear().type('Updated Title');
      cy.get('textarea[name="description"]').clear().type('Updated description.');
      cy.get('input[name="price"]').clear().type('200');
  
      // Submit the form
      cy.get('.edit-advert-button .button').click();
  
      // Wait for the update request to complete
      cy.wait('@updateAdvert');
  
      // Verify redirection to the home page or success message
      cy.url().should('eq', Cypress.config().baseUrl + '/');
    });
  
  });
  