describe('SignInComponent', () => {
    beforeEach(() => {
        cy.visit('/sign-in'); // Navigates to the sign-in page
    });

    it('allows a user to sign in', () => {
        cy.get('input[name="email"]').type('user@example.com'); // Replace with valid credentials
        cy.get('input[name="password"]').type('password'); // Replace with valid credentials
        cy.get('.button').click();
        cy.url().should('eq', Cypress.config().baseUrl + '/'); // Assuming successful sign-in redirects to the root
    });

    it('shows an error message for invalid credentials', () => {
        cy.get('input[name="email"]').type('wrong@example.com');
        cy.get('input[name="password"]').type('wrongpassword');
        cy.get('.button').click();
        cy.get('.error-message').should('contain', 'L\'e-mail ou le mot de passe n\'est pas valide');
    });
});
