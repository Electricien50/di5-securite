package com.polycoin.Offer;

import com.polycoin.Advert.AdvertRepository;
import com.polycoin.Advert.AdvertService;
import com.polycoin.Client.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OfferService {
    private final OfferRepository offerRepository;
    private final ClientRepository clientRepository;
    private final AdvertRepository advertRepository;
    private final AdvertService advertService;
    
    @Autowired
    public OfferService(OfferRepository offerRepository, ClientRepository clientRepository, AdvertRepository advertRepository, AdvertService advertService) {
        this.offerRepository = offerRepository;
        this.clientRepository = clientRepository;
        this.advertRepository = advertRepository;
        this.advertService = advertService;
    }

    // add new offer
    public void addNewOffer(Offer offer) {
        // Check if idClient exists in the database
        if(!clientRepository.existsById(offer.getIdClient())){
            throw new IllegalStateException("Client with id " + offer.getIdClient() + " does not exist");
        }

        // Check if idAdvert exists in the database
        if(!advertRepository.existsById(offer.getIdAdvert())){
            throw new IllegalStateException("Advert with id " + offer.getIdAdvert() + " does not exist");
        }

        offerRepository.save(offer);
    }

    // accept offer
    public void acceptOffer(Long idAdvert, Long idClient) {
        Offer offer = offerRepository.findByIdAdvertAndIdClient(idAdvert, idClient);
        if(offer == null) {
            throw new IllegalStateException("Offer with idAdvert " + idAdvert + " and idClient " + idClient + " does not exist");
        }

        advertService.buyAdvert(offer.getIdAdvert());;

        // Delete all offer with same idAdvert
        offerRepository.deleteAllByIdAdvert(idAdvert);

    }

    // deny offer
    public void denyOffer(Long idAdvert, Long idClient) {
        Offer offer = offerRepository.findByIdAdvertAndIdClient(idAdvert, idClient);
        if(offer == null){
            throw new IllegalStateException("Offer with idAdvert " + idAdvert + " and idClient " + idClient + " does not exist");
        }
        offerRepository.delete(offer);
    }


    // delete offer
    public void deleteOffer(Offer offer) {
        if(!offerRepository.existsByIdAdvertAndIdClient(offer.getIdAdvert(), offer.getIdClient())){
            throw new IllegalStateException("Offer with idAdvert " + offer.getIdAdvert() + " and idClient " + offer.getIdClient() + " does not exist");
        }

        offerRepository.delete(offer);
    }
}
