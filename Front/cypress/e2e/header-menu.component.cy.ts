describe('HeaderMenuComponent', () => {
    beforeEach(() => {
      cy.visit('/');
    });
  
    it('navigates to the home page when the logo or title is clicked', () => {
      cy.get('.logo').click();
      cy.url().should('eq', Cypress.config().baseUrl + '/');
    });
  
    context('when not logged in', () => {
      beforeEach(() => {
        // .........      
    });
  
      it('shows the sign-in and sign-up buttons', () => {
        cy.get('.SignNotLogged').should('be.visible');
        cy.get('.signin-button .button').should('contain', 'Se connecter');
        cy.get('.signup-button .button').should('contain', 'S\'inscrire');
      });
  
      it('navigates to the sign-in page when "Se connecter" is clicked', () => {
        cy.get('.signin-button .button').click();
        cy.url().should('include', '/sign-in');
      });
  
      it('navigates to the sign-up page when "S\'inscrire" is clicked', () => {
        cy.get('.signup-button .button').click();
        cy.url().should('include', '/sign-up');
      });
    });
  
    context('when logged in', () => {
      beforeEach(() => {
        // Connect the client
        cy.visit('http://77.197.5.43:4200/'); // Navigates to the page for create Advert
        cy.get('.button-login').click();
        cy.get('input[name="email"]').type('test@gmail.com');
        cy.get('input[name="password"]').type('test');
        cy.get('.signin-button-button').click();
    });
  
      it('shows the user\'s name and logout button', () => {
        cy.get('.profile-name .name').should('contain', 'test test');
        cy.get('.signout-button .button').should('contain', 'Se déconnecter');
      });
  
      it('navigates to the user profile page when "Mon profil" is clicked', () => {
        cy.get('.myprofile-button .button').click();
        cy.url().should('include', '/edit-profile');
      });
  
      it('navigates to the create advert page when "+ Déposer une annonce" is clicked', () => {
        cy.get('.buttonAddAdvert').click();
        cy.url().should('include', '/create-advert');
      });
  
      it('logs out the user when "Se déconnecter" is clicked', () => {
        cy.get('.signout-button .button').click();
        // Verify the logout action, e.g., by checking the URL or visible login buttons
        cy.get('.SignNotLogged').should('be.visible');
      });
    });
  });
  