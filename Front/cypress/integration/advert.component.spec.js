// cypress/integration/advert.component.spec.js

describe('AdvertComponent', () => {
    const mockAdvert = {
        title: "Voiture Economique",
        price: "15000",
        description: "Une très bonne voiture économique avec peu de kilomètres."
    };

    beforeEach(() => {
        // Mock the API call to fetch advert details
        cy.intercept('GET', '/api/advert/*', {
            statusCode: 200,
            body: mockAdvert,
        }).as('fetchAdvert');

        // Mock the user being logged in, if necessary for your app
        // For example, setting a token in localStorage
        // cy.setLocalStorage('token', 'your-mock-token');

        // Visit the advert page
        cy.visit('/advert'); // Adjust if your app uses dynamic URLs for adverts
    });

    it('displays advert details correctly', () => {
        cy.wait('@fetchAdvert');

        cy.get('.label-title').should('contain', mockAdvert.title);
        cy.get('.label-price').should('contain', `${mockAdvert.price} €`);
        cy.get('.label-description').should('contain', mockAdvert.description);
    });

    it('prompts login when trying to buy without being logged in', () => {
        // Assuming that the user's login state is checked before showing the alert
        // This might require adjusting depending on how your app handles the login state
        cy.get('.button-buy').click();

        cy.on('window:alert', (text) => {
            expect(text).to.contains('Vous devez être connecté pour acheter une annonce');
        });
    });

    it('confirms purchase when logged in and buying', () => {
        // Mock being logged in, if your app uses API calls to verify this state
        // Mock the buyAdvert API call
        cy.intercept('POST', '/api/buyAdvert/*', {
            statusCode: 200,
            body: { success: true },
        }).as('buyAdvert');

        cy.get('.button-buy').click();

        // Simulate user confirming the purchase
        cy.on('window:confirm', () => true);

        cy.wait('@buyAdvert');

        // Check for success alert message
        cy.on('window:alert', (text) => {
            expect(text).to.contains('Achat effectué avec succès');
        });
    });

});
