package com.polycoin.Client;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {

    @Query(value = "SELECT * FROM Client WHERE Client.mail = :mail AND Client.password = :password", nativeQuery = true)
    public Client isValidClient(@Param("mail") String mail, @Param("password") String password);

    @Query(value = "SELECT * FROM Client WHERE Client.mail = :mail", nativeQuery = true)
    public Client findByMail(@Param("mail") String mail);

    @Query(value = "SELECT * FROM Client WHERE Client.mail = :mail AND Client.idClient != :idClient", nativeQuery = true)
    public Client findByMailNotMe(@Param("mail") String mail, @Param("idClient") Long idClient);
}
