package com.polycoin.Client;

import com.polycoin.ApiPolycoin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.Key;
import java.util.Base64;
import java.util.List;

@Service
public class ClientService {

    private static final String ALGORITHM = "AES";
    private static final String MODE = "AES/CBC/PKCS5Padding"; // Mode de chiffrement AES avec CBC


    private final ClientRepository clientRepository;
    @Autowired
    public ClientService(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    public List<Client> getAllClients() {
        return clientRepository.findAll();
    }

    public Client getClientById(Long idClient){
        return clientRepository.findById(idClient).orElseThrow(() -> new IllegalStateException("Client with id " + idClient + " does not exist"));
    }

    public Client getClientByMail(String mail) {
        return clientRepository.findByMail(mail);
    }

    public Client isValidClient(String mail, String password) throws Exception {

        Client client = getClientByMail(mail);

        if (client == null) {
            return null;
        }
        //Load ssl key in /src/main/resources/key.txt
        // Load the SSL key from the resources directory
        byte[] keyValue = new byte[0];
        InputStream inputStream = ApiPolycoin.class.getClassLoader().getResourceAsStream("key.txt");
        if (inputStream != null) {
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
                String line;
                while ((line = reader.readLine()) != null) {
                    // Process each line from the file
                    keyValue = line.getBytes("UTF-8");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.err.println("File not found: " + "key.txt");
            return null;
        }

        //decode client password
        byte[] encryptedBytesWithIVClient = Base64.getDecoder().decode(client.getPassword());
        byte[] ivClient = new byte[16]; // Taille du vecteur d'initialisation pour AES
        System.arraycopy(encryptedBytesWithIVClient, 0, ivClient, 0, ivClient.length);
        Cipher cipherClient = Cipher.getInstance(MODE);
        SecretKeySpec keyClient = new SecretKeySpec(keyValue, ALGORITHM);
        cipherClient.init(Cipher.DECRYPT_MODE, keyClient, new IvParameterSpec(ivClient));
        byte[] decryptedValueClient = cipherClient.doFinal(encryptedBytesWithIVClient, ivClient.length, encryptedBytesWithIVClient.length - ivClient.length);
        String decodedPasswordClient = new String(decryptedValueClient, "UTF-8");

        if (!password.equals(decodedPasswordClient)) {
            return null;
        }

        return client;
    }

    public Client addNewClient(Client client) {
        // Check if mail is already used
        Client clientToCheck = clientRepository.findByMail(client.getMail());
        if (clientToCheck != null) {
            throw new IllegalStateException("Mail already exists");
        }

        //encode password
        try {
            //Load ssl key in /src/main/resources/key.txt
            // Load the SSL key from the resources directory
            byte[] keyValue = new byte[0];
            InputStream inputStream = ApiPolycoin.class.getClassLoader().getResourceAsStream("key.txt");
            if (inputStream != null) {
                try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
                    String line;
                    while ((line = reader.readLine()) != null) {
                        // Process each line from the file
                        keyValue = line.getBytes("UTF-8");
                    }
                    //
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                System.err.println("File not found: " + "key.txt");
                return null;
            }
            Cipher cipher = Cipher.getInstance(MODE);
            SecretKeySpec key = new SecretKeySpec(keyValue, ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] encryptedValue = cipher.doFinal(client.getPassword().getBytes("UTF-8"));
            byte[] iv = cipher.getIV();
            byte[] encryptedBytesWithIV = new byte[iv.length + encryptedValue.length];
            System.arraycopy(iv, 0, encryptedBytesWithIV, 0, iv.length);
            System.arraycopy(encryptedValue, 0, encryptedBytesWithIV, iv.length, encryptedValue.length);
            String encodedPassword = Base64.getEncoder().encodeToString(encryptedBytesWithIV);
            client.setPassword(encodedPassword);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return clientRepository.save(client);
    }

    public Client updateClient(Client client) {
        // Check if client exists
        Client clientToUpdate = clientRepository.findById(client.getIdClient()).orElseThrow(() -> new IllegalStateException("Client with id " + client.getIdClient() + " does not exist"));

        // Check if mail is already used
        Client clientToCheck = clientRepository.findByMailNotMe(client.getMail(), client.getIdClient());
        if (clientToCheck != null) {
            throw new IllegalStateException("Mail already exists");
        }

        clientToUpdate.setMail(client.getMail());
        clientToUpdate.setFirstName(client.getFirstName());
        clientToUpdate.setLastName(client.getLastName());
        clientToUpdate.setPassword(client.getPassword());

        return clientRepository.save(clientToUpdate);
    }
}
