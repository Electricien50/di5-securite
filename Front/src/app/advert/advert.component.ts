import { Component } from '@angular/core';
import { Advert } from '../models/advert.model';
import { ActivatedRoute } from '@angular/router';
import { AdvertService } from '../services/advert.service';
import { SharedDataService } from '../shared-data.service';

@Component({
  selector: 'app-advert',
  templateUrl: './advert.component.html',
  styleUrls: ['./advert.component.scss']
})
export class AdvertComponent {

  advert!: Advert;

  constructor(private route : ActivatedRoute,
    private advertService: AdvertService,
    private sharedDataService: SharedDataService) { 
    
      this.sharedDataService.lastPage = '/advert';
  }


  ngOnInit(): void {
    this.sharedDataService.lastPage += '/' + this.route.snapshot.paramMap.get('id');
    this.getAdvert();
  }

  getAdvert(): void {
    // Retrieve the id from the current route
    const id = this.route.snapshot.paramMap.get('id');
    if(id != null){
      this.advertService.getAdvertById(parseInt(id)).subscribe(advert => {
        this.advert = advert;
        console.log(this.advert);
      });
    }
  }


  buyAdvert() {
    // console.log("Achat de l'annonce");

    // Check if client is logged
    if(!this.sharedDataService.isLogged){
      alert("Vous devez être connecté pour acheter une annonce");
      return;
    }

    if(this.advert.status == 'NORMAL'){
      // Alert box to confirm the purchase
      if(confirm("Voulez-vous vraiment acheter cette annonce ?")){
        this.advertService.buyAdvert(this.advert.idAdvert).subscribe(bool => {
          if(bool){
            alert("Achat effectué avec succès");
            this.advert.status = 'PURCHASED';
          }else{
            alert("Erreur lors de l'achat");
          }
        })
      }
    }
  }
}
