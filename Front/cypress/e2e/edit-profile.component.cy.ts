describe('EditProfileComponent', () => {
    beforeEach(() => {

      // Mock the PUT request for updating client information
      cy.intercept('PUT', '/client', {
        statusCode: 200,
        body: {
          idClient: 1,
          firstName: 'UpdatedFirstName',
          lastName: 'UpdatedLastName',
          mail: 'updated.email@example.com',
        },
      }).as('updateClient');
  
      // Visit the edit profile page directly
      cy.visit('/edit-profile');

      // Connect the client
      cy.visit('http://77.197.5.43:4200/'); // Navigates to the page for create Advert
      cy.get('.button-login').click();
      cy.get('input[name="email"]').type('test@gmail.com');
      cy.get('input[name="password"]').type('test');
      cy.get('.signin-button-button').click();

      // Go to the page 
      cy.get('.button-edit-profile').click();
    });
  
    it('successfully updates profile information', () => {
      // Fill out the form with updated information
      cy.get('input[name="last-name"]').clear().type('UpdatedLastName');
      cy.get('input[name="first-name"]').clear().type('UpdatedFirstName');
      cy.get('input[name="email"]').clear().type('updated.email@example.com');
  
      // Submit the form
      cy.get('.editprofile-button .button').click();
  
      // Wait for the PUT request to complete
      cy.wait('@updateClient');
  
      // Verify the success alert and/or redirection
      cy.url().should('eq', Cypress.config().baseUrl + '/');
  
    });
  
    // tests to cover validation and error handling
    it('shows validation messages for incorrect input', () => {
      // for an empty last name
      cy.get('input[name="last-name"]').clear();
      cy.get('.editprofile-button .button').click();
      cy.get('.error-message').should('contain', 'Veuillez remplir tous les champs du formulaire correctement.');
  
      // first name and email validations ....
    });
  
    // Test for handling server-side errors (e.g., email already used)
    it('handles server-side errors gracefully', () => {
      // Mock the PUT request to simulate a server error (e.g., email already in use)
      cy.intercept('PUT', '/client', {
        statusCode: 400,
        body: {
          message: 'Erreur lors de la modification du profil (email déjà utilisé)',
        },
      }).as('updateClientError');
  
      // Fill and submit the form as before
      cy.get('input[name="last-name"]').clear().type('LastName');
      cy.get('input[name="first-name"]').clear().type('FirstName');
      cy.get('input[name="email"]').clear().type('test2@gmail.com');
      cy.get('.editprofile-button .button').click();
  
      // Wait for the PUT request to fail
      cy.wait('@updateClientError');
  
      cy.get('.error-message').should('contain', 'Erreur lors de la modification du profil (email déjà utilisé)');
    });
  });
  