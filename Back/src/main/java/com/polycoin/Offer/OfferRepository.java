package com.polycoin.Offer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface OfferRepository extends JpaRepository<Offer, Long> {
    @Query("SELECT CASE WHEN COUNT(o) > 0 THEN true ELSE false END FROM Offer o WHERE o.idAdvert = ?1 AND o.idClient = ?2")
    public boolean existsByIdAdvertAndIdClient(Long idAdvert, Long idClient);

    @Query("SELECT o FROM Offer o WHERE o.idAdvert = ?1 AND o.idClient = ?2")
    public Offer findByIdAdvertAndIdClient(Long idAdvert, Long idClient);
    @Modifying
    @Transactional
    @Query("DELETE FROM Offer o WHERE o.idAdvert = ?1")
    public void deleteAllByIdAdvert(Long idAdvert);
}
