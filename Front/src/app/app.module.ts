import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AccueilComponent } from './accueil/accueil.component';
import { HeaderMenuComponent } from './header-menu/header-menu.component';
import { MyAdvertsComponent } from './my-adverts/my-adverts.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SignUpComponent } from './sign-up/sign-up.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { EditAdvertComponent } from './edit-advert/edit-advert.component';
import { CreateAdvertComponent } from './create-advert/create-advert.component';
import { AdvertComponent } from './advert/advert.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { AdvertService } from './services/advert.service';



@NgModule({
  declarations: [
    AppComponent,
    AccueilComponent,
    HeaderMenuComponent,
    MyAdvertsComponent,
    SignInComponent,
    SignUpComponent,
    EditProfileComponent,
    EditAdvertComponent,
    CreateAdvertComponent,
    AdvertComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [AdvertService],
  bootstrap: [AppComponent]
})
export class AppModule { }
