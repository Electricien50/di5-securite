import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccueilComponent } from "./accueil/accueil.component";
import { HeaderMenuComponent } from "./header-menu/header-menu.component";
import { MyAdvertsComponent } from "./my-adverts/my-adverts.component";
import { SignInComponent } from "./sign-in/sign-in.component";
import { SignUpComponent } from './sign-up/sign-up.component';
import { EditProfileComponent } from "./edit-profile/edit-profile.component";
import { EditAdvertComponent } from './edit-advert/edit-advert.component';
import { CreateAdvertComponent } from './create-advert/create-advert.component';
import { AdvertComponent } from './advert/advert.component';

const routes: Routes = [
  { path: "", component: AccueilComponent, title: "P'tiCoin - Accueil" },
  { path: "header-menu", component: HeaderMenuComponent },
  { path: "my-adverts", component: MyAdvertsComponent, title: "P'tiCoin - Mes Annonces"},
  { path: "sign-in", component: SignInComponent, title: "Connexion"},
  { path: "sign-up", component: SignUpComponent, title: "Inscription"},
  { path: "edit-profile", component: EditProfileComponent, title: "Modifier profil"},
  { path: "edit-advert/:id", component: EditAdvertComponent, title: "Modifier annonce"},
  { path: "create-advert", component: CreateAdvertComponent, title: "Créer Annonce"},
  { path: "advert/:id", component: AdvertComponent, title: "P'tiCoin - Annonce x"},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
