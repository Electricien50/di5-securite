import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Advert } from '../models/advert.model';
import { SharedDataService } from '../shared-data.service';


@Injectable({
  providedIn: 'root'
})
export class AdvertService {
  serviceUrl = this.sharedDataService.getBaseUrl() + '/advert';

  constructor(private http: HttpClient,
    private sharedDataService: SharedDataService) { }

  getAvailableAdverts() : Observable<Advert[]> {
    return this.http.get<Advert[]>(this.serviceUrl + '/available');
  }

  getAvailableAdvertsByClient(idClient: number) : Observable<Advert[]> {
    return this.http.get<Advert[]>(this.serviceUrl + '/available/client/' + idClient);
  }

  getAdvertsByClient(idClient: number) : Observable<Advert[]> {
    return this.http.get<Advert[]>(this.serviceUrl + '/client/' + idClient);
  }

  buyAdvert(idAdvert: number) : Observable<Advert> {
    return this.http.get<Advert>(this.serviceUrl + '/buy/' + idAdvert);
  }

  updateAdvert(advert: Advert) : Observable<Advert> {
    return this.http.put<Advert>(this.serviceUrl, advert);
  }

  getAdvertById(id: number) : Observable<Advert> {
    return this.http.get<Advert>(this.serviceUrl + '/' + id);
  }


  // createAdvert(advert: Advert) : Boolean {
  //   const body = {
  //     title: advert.title,
  //     description: advert.description,
  //     price: advert.price,
  //     status : advert.status,
  //     picture: advert.picture,
  //     idClient: advert.idClient
  //   };

  //   this.http.post(this.serviceUrl, body).subscribe(
  //     {
  //       next: () => {
  //         console.log('Advert created');
  //       },
  //       error: (error) => {
  //         console.log('Error when creating advert : ' + error);
  //       }
  //     }
  //   );
  //   return true;
  // }
}
