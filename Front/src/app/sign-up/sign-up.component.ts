import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ClientService } from '../services/client.service';
import { Router } from '@angular/router';
import { SharedDataService } from '../shared-data.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent {
  passwordInputContent: string="";
  isLengthValid: boolean = false;
  hasUppercase: boolean = false;
  hasLowercase: boolean = false;
  hasSpecialChar: boolean = false;
  isValidMail: boolean = false;
  formValid: boolean = false;
  formSubmitted: boolean = false;
  signUpForm: FormGroup;
  emailTaken: boolean = false;


  constructor(private clientService: ClientService,
    private formBuilder: FormBuilder,
    private router: Router,
    private sharedDataService: SharedDataService) {
      this.signUpForm = this.formBuilder.group({
        lastName: ['', Validators.required],
        firstName: ['', Validators.required],
        mail: ['', Validators.required],
        password: ['', Validators.required]
      });

      this.sharedDataService.lastPage = '/sign-up';
            
  }

  ngOnInit() {
    this.updatePasswordValidation();
  }

  onSubmit() {
    // Recheck password
    this.updatePasswordValidation();

    // Appeler la/les fonctions du back pour faire le traitement
    this.formSubmitted = true;
    
    // Verify if the mail respect pattern
    if(this.signUpForm.value.mail.match(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/) == null){
      this.isValidMail = false;
      this.formValid = false;
      // alert("L'adresse mail n'est pas valide");
      return;
    }

    // Verifier que les champs sont valides
    if (this.signUpForm.invalid || !this.formValid) {
      return;
    }

    this.clientService.addClient(this.signUpForm.value.lastName, this.signUpForm.value.firstName, this.signUpForm.value.mail, this.signUpForm.value.password).subscribe({
      next: (response) => {
        if (response) {
          this.sharedDataService.setIsLogged(true);
          this.sharedDataService.setConnectedUser(response);
          this.router.navigate(['/']);
        }
      },
      error: () => {
        this.emailTaken = true;
        console.log('Error when adding client because the email is already taken');
      }
    });
  }

  updatePasswordValidation() {
    this.isLengthValid = this.passwordInputContent.length >= 8;
    this.hasUppercase = /[A-Z]/.test(this.passwordInputContent);
    this.hasLowercase = /[a-z]/.test(this.passwordInputContent);
    this.hasSpecialChar = /[!@#$%^&*(),.?":{}|<>]/.test(this.passwordInputContent);
    this.formValid = this.isLengthValid && this.hasUppercase && this.hasLowercase && this.hasSpecialChar;
  }

  submitForm() {
    this.formSubmitted = true;
    console.log("Formulaire soumis !");
  }
}
