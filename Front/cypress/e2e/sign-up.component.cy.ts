describe('SignUpComponent', () => {
    beforeEach(() => {
      // Visit the signup page before each test
      cy.visit('/sign-up');
  
      // Mock the POST request for adding a new client
      cy.intercept('POST', '/client', (req) => {
        if (req.body.mail === 'taken.email@example.com') {
          // Simulate an email already taken error
          req.reply({
            statusCode: 400,
            body: 'Email is already taken'
          });
        } else {
          // Simulate a successful signup
          req.reply({
            statusCode: 200,
            body: { idClient: 123, ...req.body }
          });
        }
      }).as('addClient');
    });
  
    it('validates password requirements', () => {
      // Type an invalid password and check validation messages
      cy.get('input[name="password"]').type('short');
      cy.get('.list-conditions').should('contain', 'Au moins 8 caractères')
        .and('contain', 'Au moins une majuscule')
        .and('contain', 'Au moins une minuscule')
        .and('contain', 'Au moins un caractère spécial');
    });
  
    it('submits the form with valid data', () => {
      // Fill out the form with valid information
      cy.get('input[name="last-name"]').type('Doe');
      cy.get('input[name="first-name"]').type('John');
      cy.get('input[name="email"]').type('valid.email@example.com');
      cy.get('input[name="password"]').type('Valid1*Password');
  
      // Submit the form
      cy.get('.button-sign-up').click();
  
      // Wait for the POST request to complete
      cy.wait('@addClient').its('response.statusCode').should('eq', 200);
  
      // Verify redirection to the home page or other success behavior
      cy.url().should('eq', Cypress.config().baseUrl + '/');
    });
  
    it('shows an error when the email is already taken', () => {
      // Fill out the form with an email that's already taken
      cy.get('input[name="last-name"]').type('Smith');
      cy.get('input[name="first-name"]').type('Emily');
      cy.get('input[name="email"]').type('taken.email@example.com');
      cy.get('input[name="password"]').type('Valid1*Password');
  
      // Submit the form
      cy.get('.button-sign-up').click();
  
      // Wait for the POST request to complete
      cy.wait('@addClient').its('response.statusCode').should('eq', 400);
  
      // Check for the error message about the email being already taken
      cy.get('.error-message').should('contain', 'Cette adresse mail est déjà utilisée.');
    });
  
  });
  