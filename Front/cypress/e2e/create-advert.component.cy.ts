describe('CreateAdvertComponent', () => {
    beforeEach(() => {
      // Connect the client
      cy.visit('http://77.197.5.43:4200/'); // Navigates to the page for create Advert
      cy.get('.button-login').click();
      cy.get('input[name="email"]').type('test@gmail.com');
      cy.get('input[name="password"]').type('test');
      cy.get('.signin-button-button').click();


      // Mock the POST request for creating an advert
      cy.intercept('POST', '/advert', {
        statusCode: 200,
        body: {
          idAdvert: 123,
          title: 'Test Advert',
          description: 'This is a test description for an advert.',
          price: 100,
          status: 'NORMAL',
          picture: 'picture.png',
          idClient: 1,
        },
      }).as('createAdvert');
    });
  
    it('successfully creates an advert', () => {
      cy.get('.buttonAddAdvert').click();

      // Fill out the form
      cy.get('input[name="title"]').type('Test Advert');
      cy.get('textarea[name="description"]').type('This is a test description for an advert.');
      cy.get('input[name="price"]').type('100');

      // Submit the form
      cy.get('.create-advert-button .button').click();
  
      // Wait for the POST request to complete
      cy.wait('@createAdvert');
  
      // Verify redirection or success message
      cy.url().should('eq', Cypress.config().baseUrl + '/');
  
      // cy.contains('Advert created').should('be.visible');
    });

    it('requires title, description, and price', () => {
        cy.get('.buttonAddAdvert').click();

        // Attempt to submit the form without filling out any fields
        cy.get('.create-advert-button .button').click();
        
        // Check for validation messages or indications that fields are required
        cy.get('.error-message').should('contain', 'L\'annonce doit avoir un titre valide');
        cy.get('.error-message').should('contain', 'L\'annonce doit avoir une description valide');
        cy.get('.error-message').should('contain', 'L\'annonce doit avoir un prix valide');
        
        // The form should not navigate away from the create-advert page
        cy.url().should('include', '/create-advert');
      });

    it('validates price to be a number', () => {
      cy.get('.buttonAddAdvert').click();

      // Fill out the form with an invalid price (non-numeric)
      cy.get('input[name="title"]').type('Valid Title');
      cy.get('textarea[name="description"]').type('Valid description.');
      cy.get('input[name="price"]').type('NotANumber');
  
      // Submit the form
      cy.get('.create-advert-button .button').click();
  
      // Check for a validation message related to the price input
      cy.get('.error-message').should('contain', 'L\'annonce doit avoir un prix valide');
      
      // The form should not navigate away, indicating it was not submitted due to validation failure
      cy.url().should('include', '/create-advert');
    });
  
  });
  