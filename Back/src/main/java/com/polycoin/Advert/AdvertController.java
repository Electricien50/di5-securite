package com.polycoin.Advert;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/advert")
public class AdvertController {
    private final AdvertService advertService;

    @Autowired
    public AdvertController(AdvertService advertService) {
        this.advertService = advertService;
    }

    @GetMapping
    @RequestMapping("/all")
    public List<Advert> getAllAdverts() {
        return advertService.getAllAdverts();
    }

    @GetMapping
    @RequestMapping("/{idAdvert}")
    @ResponseBody
    public Advert getAdvertById(@PathVariable("idAdvert") Long idAdvert){
        return advertService.getAdvertById(idAdvert);
    }

    @GetMapping
    @RequestMapping("/available")
//    public List<Advert> getAvailableAdverts() {
//        return advertService.getAvailableAdverts();
//    }
    public ResponseEntity<List<Advert>> getAvailableAdverts() {

        try {
            List<Advert> adverts = advertService.getAvailableAdverts();
            return new ResponseEntity<List<Advert>>(adverts, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping
    @RequestMapping("/available/{idClient}/{idAdvert}")
    @ResponseBody
    public Advert getAdvertByClientAndId(@PathVariable("idClient") Long idClient, @PathVariable("idAdvert") Long idAdvert){
        return advertService.getAdvertByClientAndId(idClient, idAdvert);
    }

    @GetMapping
    @RequestMapping("/client/{idClient}")
    public List<Advert> getAdvertsByClient(@PathVariable("idClient") Long idClient) {
        return advertService.getAdvertsByClient(idClient);
    }

    @GetMapping
    @RequestMapping("/available/client/{idClient}")
    public List<Advert> getAvailableAdvertsByClient(@PathVariable("idClient") Long idClient) {
        return advertService.getAvailablAdvertsByClient(idClient);
    }

    @PostMapping
    public void addNewAdvert(@RequestBody Advert advert) {
        advertService.addNewAdvert(advert);
    }

    // Buy advert
    @PutMapping
    @RequestMapping("/buy/{idAdvert}")
    public boolean buyAdvert(@PathVariable("idAdvert") Long idAdvert) {
        return advertService.buyAdvert(idAdvert);
    }

    @PutMapping
    public ResponseEntity<Advert> updateAdvert(@RequestBody Advert advert) {
        try {
            Advert savedAdvert = advertService.updateAdvert(advert);
            return new ResponseEntity<>(savedAdvert, HttpStatus.CREATED);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @DeleteMapping
    public void deleteAdvert(@RequestBody Advert advert) {
        advertService.deleteAdvert(advert);
    }
}
