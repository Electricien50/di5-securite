
describe('AdvertComponent', () => {
    const mockAdvert = {
        idAdvert: 1,
        idClient: 1,
        title: "Titre de Test",
        price: "666",
        description: "Description de Test",
    };


    beforeEach(() => {
        // Mock the API call to fetch advert details
        cy.intercept('GET', '/api/advert/*', {
            statusCode: 200,
            body: mockAdvert,
        }).as('fetchAdvert');

        // Go to the test advert page
        cy.visit('http://77.197.5.43:4200/advert/1');
    });

    it('displays advert details correctly', () => {
        // cy.wait('@fetchAdvert');

        cy.get('.label-title').should('contain', mockAdvert.title);
        cy.get('.label-price').should('contain', `${mockAdvert.price} €`);
        cy.get('.label-description').should('contain', mockAdvert.description);
    });

    it('prompts login when trying to buy without being logged in', () => {
            cy.get('.button-buy').click();

        cy.on('window:alert', (text) => {
            expect(text).to.contains('Vous devez être connecté pour acheter une annonce');
        });
    });

    it('confirms purchase when logged in and buying', () => {
        // Go to the login page
        cy.get('.button-login').click();
        cy.get('input[name="email"]').type('test@gmail.com');
        cy.get('input[name="password"]').type('test');
        cy.get('.signin-button-button').click();

        // Mock being logged in, if your app uses API calls to verify this state
        // Mock the buyAdvert API call
        cy.intercept('POST', '/api/buyAdvert/*', {
            statusCode: 200,
            body: { success: true },
        }).as('buyAdvert');

        cy.get('.button-buy').click();

        // Simulate user confirming the purchase
        cy.on('window:confirm', () => true);

        // Check for success alert message
        cy.on('window:alert', (text) => {
            expect(text).to.contains('Achat effectué avec succès');
        });
    });

});
