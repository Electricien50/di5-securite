describe('AccueilComponent', () => {
    beforeEach(() => {
        cy.visit('http://77.197.5.43:4200/'); // Navigates to the root of the app which is mapped to AccueilComponent
    });

    it('displays the header and available adverts', () => {
        cy.get('app-header-menu').should('exist');
        cy.get('.AvailableAdvertsTitleText').contains('Les annonces disponibles');
        cy.get('.Advert').should('have.length.at.least', 1);
    });

    it('navigates through adverts using arrows', () => {
        cy.get('.RightArrowImage').click();
        cy.wait(500); // Wait for any animations or data loading
        cy.get('.LeftArrowImage').click();
    });

    it('opens an advert detail on click', () => {
        cy.get('.Advert').first().click();
        cy.url().should('include', '/advert'); // This assumes that clicking an advert navigates to its details
    });
});
