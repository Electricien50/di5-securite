package com.polycoin.Advert;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AdvertRepository extends JpaRepository<Advert, Long> {

    @Query(value = "SELECT * FROM Advert WHERE Advert.status ='NORMAL'", nativeQuery = true)
    public List<Advert> getAvailableAdverts();

    @Query(value = "SELECT * FROM Advert, Client WHERE Client.idClient = :idClient AND Advert.idAdvert = :idAdvert", nativeQuery = true)
    public Advert findByClientAndId(@Param("idClient") Long idClient, @Param("idAdvert") Long idAdvert);

    @Query(value = "SELECT * FROM Advert WHERE Advert.idClient = :idClient ORDER BY Advert.status ASC", nativeQuery = true)
    public List<Advert> findByClient(@Param("idClient") Long idClient);

    @Query(value = "SELECT * FROM Advert WHERE Advert.idClient = :idClient AND Advert.status ='NORMAL'", nativeQuery = true)
    public List<Advert> findAvailablByClient(@Param("idClient") Long idClient);
}
