package com.polycoin.Advert;

import com.polycoin.Client.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdvertService {
    private final AdvertRepository advertRepository;
    private final ClientRepository clientRepository;

    @Autowired
    public AdvertService(AdvertRepository advertRepository, ClientRepository clientRepository) {
        this.advertRepository = advertRepository;
        this.clientRepository = clientRepository;
    }


    // Get all adverts
    public List<Advert> getAllAdverts() {
        return advertRepository.findAll();
    }

    // Get advert by id
    public Advert getAdvertById(Long idAdvert){
        return advertRepository.findById(idAdvert).orElseThrow(() -> new IllegalStateException("Advert with id " + idAdvert + " does not exist"));
    }

    // Get available adverts
    public List<Advert> getAvailableAdverts() {
        return advertRepository.getAvailableAdverts();
    }

    // Get advert by client and id
    public Advert getAdvertByClientAndId(Long idClient, Long idAdvert){
        return advertRepository.findByClientAndId(idClient, idAdvert);
    }

    public List<Advert> getAdvertsByClient(Long idClient) {
        return advertRepository.findByClient(idClient);
    }

    // Get adverts by client
    public List<Advert> getAvailablAdvertsByClient(Long idClient) {
        return advertRepository.findAvailablByClient(idClient);
    }

    // Add new advert
    public void addNewAdvert(Advert advert) {
        // Check if idClient exists in the database
        if(!clientRepository.existsById(advert.getIdClient())){
            throw new IllegalStateException("Client with id " + advert.getIdClient() + " does not exist");
        }

        advertRepository.save(advert);
    }

    // Buy advert
    public boolean buyAdvert(Long idAdvert) {
        Advert advert = advertRepository.findById(idAdvert).orElseThrow(() -> new IllegalStateException("Advert with id " + idAdvert + " does not exist"));

        if(advert.getStatus().equals("NORMAL")){
            advert.setStatus("PURCHASED");
            advertRepository.save(advert);
            return true;
        }
        else{
            throw new IllegalStateException("Advert with id " + idAdvert + " is not available");
        }
    }

    public Advert updateAdvert(Advert advert) {
        // Check if advert exists
        Advert advertToUpdate = advertRepository.findById(advert.getIdAdvert()).orElseThrow(() -> new IllegalStateException("Advert with id " + advert.getIdAdvert() + " does not exist"));

        advertToUpdate.setTitle(advert.getTitle());
        advertToUpdate.setDescription(advert.getDescription());
        advertToUpdate.setPrice(advert.getPrice());
        advertToUpdate.setStatus(advert.getStatus());
        advertToUpdate.setPicture(advert.getPicture());

        return advertRepository.save(advertToUpdate);
    }

    // Delete advert
    public void deleteAdvert(Advert advert) {
        if(!advertRepository.existsById(advert.getIdAdvert())){
            throw new IllegalStateException("Advert with id " + advert.getIdAdvert() + " does not exist");
        }

        advertRepository.delete(advert);
    }
}
