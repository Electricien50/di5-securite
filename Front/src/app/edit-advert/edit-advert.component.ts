import { Component } from '@angular/core';
import { NONE_TYPE } from '@angular/compiler';
import { Advert } from '../models/advert.model';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AdvertService } from '../services/advert.service';
import { HttpClient } from '@angular/common/http';
import { SharedDataService } from '../shared-data.service';
import { get } from 'cypress/types/lodash';

@Component({
  selector: 'app-edit-advert',
  templateUrl: './edit-advert.component.html',
  styleUrls: ['./edit-advert.component.scss']
})
export class EditAdvertComponent {
  formSubmitted: boolean = false;
  formValid : boolean = true;
  advert!: Advert;
  advertForm: FormGroup;

  constructor(private advertService: AdvertService,
    private formBuilder: FormBuilder,
    private router: Router,
    private http: HttpClient,
    private sharedDataService: SharedDataService,
    private route: ActivatedRoute) {
      this.advertForm = this.formBuilder.group({
        // title: [this.advert.title, Validators.required],
        // description: [this.advert.description, Validators.required],
        // price: [this.advert.price, Validators.required],
        // picture: ['']
        title: ['', Validators.required],
        description: ['', Validators.required],
        price: ['', Validators.required],
        picture: ['']
      });

      this.sharedDataService.lastPage = '/edit-advert';

            
  }


  ngOnInit() {
    // Wait for the advert to be retrieved
    setTimeout(() => {
      this.getAdvert();
    }, 1000);

    console.log(this.advert);

    // Fill form with advert values
    this.advertForm.setValue({
      title: this.advert.title,
      description: this.advert.description,
      price: this.advert.price,
      picture: this.advert.picture
    });
  }

  getAdvert(): void {
    // Retrieve the id from the current route
    const id = this.route.snapshot.paramMap.get('id');
    console.log(id);
    if(id != null){
      this.advertService.getAdvertById(parseInt(id)).subscribe(advert => {
        this.advert = advert;
        console.log("coucou" + this.advert.title);
      });
    }
  }

  getMockAdvert(): Advert {
    const advert: Advert = {
      idAdvert: 1,
      title: 'Advert',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum commodo elit justo, vel ornare metus ornare eu. Duis in nibh massa. Aliquam erat volutpat. Vestibulum ac lorem ac sem pellentesque lacinia. Morbi viverra ut quam eu euismod. In hac habitasse platea dictumst. Etiam eu eleifend ipsum. Suspendisse at velit sit amet urna feugiat molestie id at arcu. Sed sit amet urna accumsan, egestas arcu ac, egestas velit. Cras eu molestie odio. In hac habitasse platea dictumst. Ut euismod mauris ut urna rutrum, vel hendrerit neque ultrices. Sed tincidunt sem nisi, quis pretium sem condimentum nec. Ut placerat malesuada augue vel tincidunt. Etiam finibus pulvinar efficitur. Maecenas non dolor cursus, venenatis sapien in, imperdiet felis. Phasellus eu convallis enim. In mollis pellentesque efficitur. Nullam sed lacus vel tellus ultricies vehicula et ac sem. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nullam vel sagittis ex, in sagittis felis. Sed ac dignissim mauris. In elementum orci vel odio iaculis, eu mollis risus placerat. Nam fringilla et purus et finibus. Cras lobortis lacus sed ipsum elementum, in elementum est porta. Maecenas quam augue, sollicitudin maximus ligula eu, semper tempus sem. Pellentesque eget nisi turpis. Integer ullamcorper nunc nibh, quis mollis nisl pretium a. Suspendisse id commodo nisl, a sollicitudin quam. Donec et vehicula felis, ut tempor urna. Nulla at odio felis. Vestibulum consequat, ipsum id dictum tincidunt, enim dui euismod est, eget condimentum metus velit dignissim lectus. Ut commodo sem vel dui blandit, a pellentesque libero fermentum. Donec vestibulum luctus aliquet. In pretium sem posuere sollicitudin egestas. Aliquam scelerisque libero ac nunc gravida posuere. In maximus lectus ac tellus eleifend, non pellentesque ipsum cursus. Praesent a tellus id mauris hendrerit mattis. Quisque eleifend tempus massa a varius. Curabitur ex erat, semper sit amet faucibus nec, consectetur eget nibh. Pellentesque sed sem ut nulla vehicula commodo. Proin rutrum imperdiet ultricies. Ut nec nulla a risus sodales auctor. Phasellus consequat a justo vitae ornare. Nam id augue risus. Phasellus urna ipsum, lacinia id aliquet ac, ultricies et enim. Nam ac dapibus eros. Duis vel aliquam diam. Duis nec felis a urna vulputate consectetur vitae eu magna. Nam sollicitudin mauris quis euismod volutpat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Mauris pellentesque dignissim pulvinar.',
      price: 450,
      status: 'Statut',
      idClient: 1
    };
    return advert
  }


  submitForm() {
    this.formSubmitted = true;
    console.log("Formulaire soumis !");
  }

  onSubmit() {
    this.formSubmitted = true;
    this.formValid = true;

    // Retrieve form values
    this.advert.title = this.advertForm.get('title')?.value;
    this.advert.description = this.advertForm.get('description')?.value;
    this.advert.price = this.advertForm.get('price')?.value;
    this.advert.picture = this.advertForm.get('picture')?.value;

    if(this.advertForm.valid){
      this.advertService.updateAdvert(this.advert).subscribe(
        {
          next: (response) => {
            if (response) {
              alert("Annonnce modifiée avec succès")
              this.router.navigate(['/']);
            }
          },
          error: () => {
          }
        });
    }
    else{
      this.formValid = false;
    };
  }
}
