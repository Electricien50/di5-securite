import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ClientService } from '../services/client.service';
import { Router } from '@angular/router';
import { SharedDataService } from '../shared-data.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent {
  signInForm: FormGroup;
  formSubmitted = false;
  formValid = false;
  
  constructor(private clientService: ClientService,
    private formBuilder: FormBuilder,
    private router: Router,
    private sharedDataService: SharedDataService) {
      this.signInForm = this.formBuilder.group({
        mail: ['', Validators.required],
        password: ['', Validators.required]
      });

      // this.sharedDataService.lastPage = '/sign-in';
            
  }

  


  onSubmit() {
    // Appeler la/les fonctions du back pour faire le traitement
    this.formSubmitted = true;
    
    // Verifier que les champs sont valides
    if (this.signInForm.invalid) {
      return;
    }

    this.clientService.isValidClient(this.signInForm.value.mail, this.signInForm.value.password).subscribe({
      next: (response) => {
        if (response) {
          this.formValid = true;
          this.sharedDataService.setIsLogged(true);
          this.sharedDataService.setConnectedUser(response);
          this.router.navigate([this.sharedDataService.lastPage]);
        } else {
          console.log('Invalid client');
        }
      },
      error: (error) => {
        console.log('Error when checking client : ' + error);
      }
    });
    }
}
