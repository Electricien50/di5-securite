describe('SignInComponent', () => {
    beforeEach(() => {
        cy.visit('http://77.197.5.43:4200/'); // Navigates to the page for create Advert
        cy.get('.button-login').click();
    });

    it('allows a user to sign in', () => {
        cy.get('input[name="email"]').type('test@gmail.com');
        cy.get('input[name="password"]').type('test');
        // Specify the button within the .signin-box form to ensure uniqueness
        cy.get('.signin-box .button').click();
        cy.url().should('eq', Cypress.config().baseUrl + '/');
    });

    it('shows an error message for invalid credentials', () => {
        cy.get('input[name="email"]').type('wrong@example.com');
        cy.get('input[name="password"]').type('wrongpassword');
        // Again, specify the button within the .signin-box form to ensure uniqueness
        cy.get('.signin-box .button').click();
        cy.get('.error-message').should('contain', 'L\'e-mail ou le mot de passe n\'est pas valide');
    });
});
