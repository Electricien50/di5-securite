package com.polycoin.Offer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/offer")
public class OfferController {
    private final OfferService offerService;
    @Autowired
    public OfferController(OfferService offerService) {
        this.offerService = offerService;
    }


    @PostMapping
    public void addNewOffer(@RequestBody Offer offer) {
        offerService.addNewOffer(offer);
    }

    @PutMapping
    @RequestMapping("/accept/{idAdvert}/{idClient}")
    public void acceptOffer(@PathVariable("idAdvert") Long idAdvert, @PathVariable("idClient") Long idClient) {
        offerService.acceptOffer(idAdvert, idClient);
    }

    @PutMapping
    @RequestMapping("/deny/{idAdvert}/{idClient}")
    public void denyOffer(@PathVariable("idAdvert") Long idAdvert, @PathVariable("idClient") Long idClient) {
        offerService.denyOffer(idAdvert, idClient);
    }

    @DeleteMapping
    public void deleteOffer(@RequestBody Offer offer) {
        offerService.deleteOffer(offer);
    }
}
