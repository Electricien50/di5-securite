package com.polycoin.Advert;

import javax.persistence.*;

@Entity
@Table
public class Advert {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idAdvert", updatable = false, nullable = false)
    private Long idAdvert;
    private String title;
    private String description;
    private Double price;
    private String status;
    private String picture;
    private Long idClient;

    public Advert() {
    }

    public Advert(Long idAdvert, String title, String description, Double price, String status, String picture, Long idClient) {
        this.idAdvert = idAdvert;
        this.title = title;
        this.description = description;
        this.price = price;
        this.status = status;
        this.picture = picture;
        this.idClient = idClient;
    }

    public Long getIdAdvert() {
        return idAdvert;
    }

    public void setIdAdvert(Long idAdvert) {
        this.idAdvert = idAdvert;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }



    public Long getIdClient() {
        return idClient;
    }

    public void setIdClient(Long idClient) {
        this.idClient = idClient;
    }

    @Override
    public String toString() {
        return "Advert" + "\n" +
                "idAdvert=" + idAdvert + "\n" +
                "title='" + title + '\'' + "\n" +
                "description='" + description + '\'' + "\n" +
                "price=" + price + "\n" +
                "status='" + status + '\'' + "\n" +
                "picture='" + picture + '\'' + "\n";
    }

}
