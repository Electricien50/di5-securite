import { Component } from '@angular/core';
import { AdvertService } from '../services/advert.service';
import { Advert } from '../models/advert.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SharedDataService } from '../shared-data.service';
import { connect } from 'rxjs';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-create-advert',
  templateUrl: './create-advert.component.html',
  styleUrls: ['./create-advert.component.scss']
})
export class CreateAdvertComponent {
  advertForm: FormGroup;
  formSubmitted = false;
  formValid = true;
  isTitleValid = true;
  isDescriptionValid = true;
  isPriceValid = true;
  isLogged = false;
  
  constructor(private advertService: AdvertService,
    private formBuilder: FormBuilder,
    private router: Router,
    private http: HttpClient,
    private sharedDataService: SharedDataService) {
      this.isLogged = this.sharedDataService.isLogged;
      
      this.advertForm = this.formBuilder.group({
        title: ['', Validators.required],
        description: ['', Validators.required],
        price: ['', Validators.required],
        picture: ['']
      });

      this.sharedDataService.lastPage = '/create-advert';
  }

  


  onSubmit() {
    // If title is empty
    if(this.advertForm.get('title')?.value == ""){
      this.isTitleValid = false;
      this.formValid = false;
    }

    // If description is empty
    if(this.advertForm.get('description')?.value == ""){
      this.isDescriptionValid = false;
      this.formValid = false;
    }

    // If price is empty
    if(this.advertForm.get('price')?.value == ""){
      this.isPriceValid = false;
      this.formValid = false;
    }

    // If price is not a number
    if(isNaN(this.advertForm.get('price')?.value)){
      this.isPriceValid = false;
      this.formValid = false;
    }

    // If form is not valid
    if(this.advertForm.invalid){
      this.formValid = false;
      console.log('Form is not valid');
    }

    if(!this.formValid){
      return;
    }

    // Recuperer les valeurs du formulaire
    const advert: Advert = {
      title: this.advertForm.get('title')?.value,
      description: this.advertForm.get('description')?.value,
      price: this.advertForm.get('price')?.value,
      status: 'NORMAL',
      picture: this.advertForm.get('picture')?.value,
      idClient: this.sharedDataService.getConnectedUser().idClient
    };


    // Appeler la/les fonctions du back pour faire le traitement
    const body = {
      title: advert.title,
      description: advert.description,
      price: advert.price,
      status : advert.status,
      picture: advert.picture,
      idClient: advert.idClient
    };

    const serviceUrl = this.sharedDataService.getBaseUrl() + '/advert';

    this.http.post(serviceUrl, body).subscribe(
      {
        next: () => {
          console.log('Advert created');
          this.router.navigate(['/']);
        },
        error: (error) => {
          console.log('Error when creating advert : ' + error);
        }
      }
    );
  }
}
