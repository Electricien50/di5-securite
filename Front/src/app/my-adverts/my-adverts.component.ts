import { Component } from '@angular/core';
import { Advert } from '../models/advert.model';
import { HeaderMenuComponent } from '../header-menu/header-menu.component';
import { SharedDataService } from '../shared-data.service';
import { AdvertService } from '../services/advert.service';
import { Router } from '@angular/router';
import { state } from '@angular/animations';


@Component({
  selector: 'app-my-adverts',
  templateUrl: './my-adverts.component.html',
  styleUrls: ['./my-adverts.component.scss']
})
export class MyAdvertsComponent {
  constructor(private sharedDataService: SharedDataService,
    private advertService : AdvertService,
    private router : Router) {
      this.sharedDataService.lastPage = '/my-adverts';
    }

  adverts: Advert[] = [];
  isLogged: boolean = false;
  i : number = 0;
  min : boolean = true
  max : boolean = false;



  ngOnInit(): void {
    // this.getMockAdverts();
    this.getAdverts();
    this.isLogged = this.sharedDataService.isLogged;
  }

  left(){
    if(!this.min){
      this.i -= 2;
      if(this.i==0){
        this.min = true;
      }
      this.max = false;
    }
  }

  right(){
    if(!this.max){
      this.i += 2;
      if(this.i==this.adverts.length-1){
        this.max = true;
      }
      this.min = false;
    }
  }


  getMockAdverts(): void {
    const advert1: Advert = {
      idAdvert: 1,
      title: 'Advert 1',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum commodo elit justo, vel ornare metus ornare eu. Duis in nibh massa. Aliquam erat volutpat. Vestibulum ac lorem ac sem pellentesque lacinia. Morbi viverra ut quam eu euismod. In hac habitasse platea dictumst. Etiam eu eleifend ipsum. Suspendisse at velit sit amet urna feugiat molestie id at arcu. Sed sit amet urna accumsan, egestas arcu ac, egestas velit. Cras eu molestie odio. In hac habitasse platea dictumst. Ut euismod mauris ut urna rutrum, vel hendrerit neque ultrices. Sed tincidunt sem nisi, quis pretium sem condimentum nec. Ut placerat malesuada augue vel tincidunt. Etiam finibus pulvinar efficitur. Maecenas non dolor cursus, venenatis sapien in, imperdiet felis. Phasellus eu convallis enim. In mollis pellentesque efficitur. Nullam sed lacus vel tellus ultricies vehicula et ac sem. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nullam vel sagittis ex, in sagittis felis. Sed ac dignissim mauris. In elementum orci vel odio iaculis, eu mollis risus placerat. Nam fringilla et purus et finibus. Cras lobortis lacus sed ipsum elementum, in elementum est porta. Maecenas quam augue, sollicitudin maximus ligula eu, semper tempus sem. Pellentesque eget nisi turpis. Integer ullamcorper nunc nibh, quis mollis nisl pretium a. Suspendisse id commodo nisl, a sollicitudin quam. Donec et vehicula felis, ut tempor urna. Nulla at odio felis. Vestibulum consequat, ipsum id dictum tincidunt, enim dui euismod est, eget condimentum metus velit dignissim lectus. Ut commodo sem vel dui blandit, a pellentesque libero fermentum. Donec vestibulum luctus aliquet. In pretium sem posuere sollicitudin egestas. Aliquam scelerisque libero ac nunc gravida posuere. In maximus lectus ac tellus eleifend, non pellentesque ipsum cursus. Praesent a tellus id mauris hendrerit mattis. Quisque eleifend tempus massa a varius. Curabitur ex erat, semper sit amet faucibus nec, consectetur eget nibh. Pellentesque sed sem ut nulla vehicula commodo. Proin rutrum imperdiet ultricies. Ut nec nulla a risus sodales auctor. Phasellus consequat a justo vitae ornare. Nam id augue risus. Phasellus urna ipsum, lacinia id aliquet ac, ultricies et enim. Nam ac dapibus eros. Duis vel aliquam diam. Duis nec felis a urna vulputate consectetur vitae eu magna. Nam sollicitudin mauris quis euismod volutpat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Mauris pellentesque dignissim pulvinar.',
      price: 450,
      status: 'Statut 1',
      picture: 1,
      idClient: 1
    };

    const advert2: Advert = {
      idAdvert: 2,
      title: 'Advert 2',
      description: 'Description 2',
      price: 360,
      status: 'Statut 2',
      picture: 1,
      idClient: 1
    };

    this.adverts = [advert1, advert2];
  }

  getAdverts(): void {
    this.advertService.getAdvertsByClient(this.sharedDataService.getConnectedUser().idClient).subscribe(
      (adverts) => {
        this.adverts = adverts;
        console.log(adverts);
      }
    );
  }

  onClick(advert: Advert): void {
    this.sharedDataService.sharedAdvert = advert;
    this.router.navigate(['/edit-advert', advert.idAdvert]);
  }
}
