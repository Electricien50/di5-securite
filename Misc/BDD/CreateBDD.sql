DROP TABLE IF EXISTS Offer;
DROP TABLE IF EXISTS Advert;
DROP TABLE IF EXISTS Client;

CREATE TABLE Client(
   idClient INT AUTO_INCREMENT,
   firstName VARCHAR(50),
   lastName VARCHAR(50),
   mail VARCHAR(100),
   password VARCHAR(50),
   PRIMARY KEY(idClient)
);

CREATE TABLE Advert(
   idAdvert INT AUTO_INCREMENT,
   title VARCHAR(100),
   description VARCHAR(500),
   price DOUBLE,
   status VARCHAR(20),
   picture LONGBLOB,
   idClient INT NOT NULL,
   PRIMARY KEY(idAdvert),
   FOREIGN KEY(idClient) REFERENCES Client(idClient)
);

CREATE TABLE Offer(
   idClient INT,
   price DOUBLE,
   idAdvert INT NOT NULL,
   PRIMARY KEY(idClient),
   FOREIGN KEY(idClient) REFERENCES Client(idClient),
   FOREIGN KEY(idAdvert) REFERENCES Advert(idAdvert)
);
