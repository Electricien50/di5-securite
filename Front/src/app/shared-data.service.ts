import { Injectable } from '@angular/core';
import { Client } from './models/client.models';
import { Advert } from './models/advert.model';

@Injectable({
  providedIn: 'root',
})

export class SharedDataService {
  isLogged: boolean = false;
  connectedUser : Client = {
    idClient: -1,
    firstName: '',
    lastName: '',
    mail: ''
  }
  // baseUrl: string = 'http://77.197.5.43:8080';
  baseUrl: string = 'http://localhost:8080';

  sharedAdvert?: Advert;

  lastPage: string = '';

  constructor() { }

  getIsLogged(): boolean {
    return this.isLogged;
  }

  setIsLogged(isLogged: boolean): void {
    this.isLogged = isLogged;
  }

  getConnectedUser(): Client {
    return this.connectedUser;
  }

  setConnectedUser(connectedUser: Client): void {
    this.connectedUser = connectedUser;
  }

  getBaseUrl(): string {
    return this.baseUrl;
  }

  setBaseUrl(baseUrl: string): void {
    this.baseUrl = baseUrl;
  }

}

