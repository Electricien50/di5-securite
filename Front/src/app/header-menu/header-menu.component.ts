import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SharedDataService } from '../shared-data.service';

@Component({
  selector: 'app-header-menu',
  templateUrl: './header-menu.component.html',
  styleUrls: ['./header-menu.component.scss']
})
export class HeaderMenuComponent {
  isLogged: boolean = false;
  connectedUser = this.sharedDataService.getConnectedUser();

  constructor(private router: Router,
    private sharedDataService: SharedDataService) {}



  ngOnInit(): void {
    this.isLogged = this.sharedDataService.getIsLogged();
  }

  goAccueil(): void {
    this.router.navigate(['/']);
  }

  goCreateAdvert(): void {
    this.router.navigate(['/create-advert']);
  }

  goSignIn(): void {
    this.router.navigate(['/sign-in']);
  }

  goSignUp(): void {
    this.router.navigate(['/sign-up']);
  }

  goMyAdverts(): void {
    this.router.navigate(['/my-adverts']);
  }

  goEditProfile(): void {
    this.router.navigate(['/edit-profile']);
  }

  disconnect(): void {
    this.sharedDataService.setIsLogged(false);
    this.sharedDataService.setConnectedUser({
      idClient: -1,
      firstName: '',
      lastName: '',
      mail: ''
    });

    this.ngOnInit();
  }

}
