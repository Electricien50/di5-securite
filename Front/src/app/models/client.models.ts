export class Client {
    idClient?: any;
    firstName?: any; 
    lastName?: any; 
    mail?: any;
    password?: any;

    constructor(idClient?: any, firstName?: any, lastName?: any, mail?: any, password?: any) {
        this.idClient = idClient;
        this.firstName = firstName;
        this.lastName = lastName;
        this.mail = mail;
        this.password = password;
    }
} 
