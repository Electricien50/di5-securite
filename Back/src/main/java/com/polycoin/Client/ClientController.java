package com.polycoin.Client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/client")
public class ClientController {
    private final ClientService clientService;
    @Autowired
    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @GetMapping
    @RequestMapping("/all")
    public List<Client> getAllClients() {
        return clientService.getAllClients();
    }

    @GetMapping
    @RequestMapping("/{idClient}")
    public Client getClientById(@PathVariable("idClient") Long idClient){
        return clientService.getClientById(idClient);
    }

    @GetMapping
    @RequestMapping("/isValid/{mail}/{password}")
    public Client isValidClient(@PathVariable("mail") String mail, @PathVariable("password") String password) throws Exception {
        return clientService.isValidClient(mail, password);
    }

    @PostMapping
    public ResponseEntity<Client> addNewClient(@RequestBody Client client) {
        try {
            Client savedClient = clientService.addNewClient(client);
            return new ResponseEntity<>(savedClient, HttpStatus.CREATED);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping
    public ResponseEntity<Client> updateClient(@RequestBody Client client) {
        try {
            Client updatedClient = clientService.updateClient(client);
            return new ResponseEntity<>(updatedClient, HttpStatus.CREATED);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
