# Lien du GitLab

https://gitlab.com/kiloudu14/di5-conduite-test

# Projet Polytech'Tours - Application de vente d'articles d'occasion

Ce dépôt contient le code source de l'application web développée dans le cadre du projet Polytech'Tours. L'objectif de ce projet est de créer une plateforme sécurisée permettant la vente d'articles d'occasion au sein de l'établissement.

## Fonctionnalités principales

- Gestion des annonces : Permet aux utilisateurs de créer, modifier et supprimer des annonces.
- Gestion des utilisateurs : Fonctionnalités d'inscription, connexion et gestion de profil.
- Transactions d'achat : Processus de paiement sécurisé pour les acheteurs et les vendeurs.

## Technologies utilisées

- Frontend : Angular
- Backend : Spring Boot
- Base de données : MariaDB

## Lien du site à ouvrir sur Mozzila Firefox

- http://77.197.5.43:4200/

## Pour lancer Cypress

- Ouvrir un terminal dans le dossier front
- Taper la commande `npx cypress open`