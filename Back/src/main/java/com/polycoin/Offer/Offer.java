package com.polycoin.Offer;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class Offer {
    @Id
    private Long idClient;
    private Double price;
    private Long idAdvert;

    public Offer() {
    }

    public Offer(Long idClient, Double price, Long idAdvert) {
        this.idClient = idClient;
        this.price = price;
        this.idAdvert = idAdvert;
    }

    public Long getIdClient() {
        return idClient;
    }

    public void setIdClient(Long idClient) {
        this.idClient = idClient;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Long getIdAdvert() {
        return idAdvert;
    }

    public void setIdAdvert(Long idAdvert) {
        this.idAdvert = idAdvert;
    }

    @Override
    public String toString() {
        return "Offer" + "\n" +
                "idClient=" + idClient + "\n" +
                "price=" + price + "\n" +
                "idAdvert=" + idAdvert + "\n";
    }

}
