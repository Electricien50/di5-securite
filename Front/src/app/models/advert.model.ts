export class Advert {
  idAdvert?: any;
  title?: any;
  description?: any;
  price?: any;
  status?: any;
  picture?: any;
  idClient?: any;

  constructor(idAdvert?: any, title?: any, description?: any, price?: any, status?: any, picture?: any, idClient?: any) {
    this.idAdvert = idAdvert;
    this.title = title;
    this.description = description;
    this.price = price;
    this.status = status;
    this.picture = picture;
    this.idClient = idClient;
  }
}
