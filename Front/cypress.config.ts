import { defineConfig } from "cypress";

export default defineConfig({
  projectId: 'r65t55',
  component: {
    devServer: {
      framework: "angular",
      bundler: "webpack",
    },
    specPattern: "**/*.cy.ts",
  },

  e2e: {
    baseUrl: 'http://77.197.5.43:4200',
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },
});
