import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SharedDataService } from '../shared-data.service';
import { Client } from '../models/client.models';
import * as CryptoJS from 'crypto-js';

@Injectable({
  providedIn: 'root'
})
export class ClientService {
  serviceUrl = this.sharedDataService.getBaseUrl() + '/client';

  constructor(private http: HttpClient,
    private sharedDataService: SharedDataService) { }

  isValidClient(mail: string, password: string) {
    return this.http.get(this.serviceUrl + '/isValid/' + mail + '/' + password);
  }

  addClient(lastName: string, firstName: string, mail: string, password: string) {
    return this.http.post(this.serviceUrl, {
      lastName: lastName,
      firstName: firstName,
      mail: mail,
      password: password
    });
  }

  updateClient(client: Client) {
    return this.http.put(this.serviceUrl, client);
  }
}
