describe('MyAdvertsComponent', () => {
    beforeEach(() => {
      // Connect the client
      cy.visit('http://77.197.5.43:4200/'); // Navigates to the page for create Advert
      cy.get('.button-login').click();
      cy.get('input[name="email"]').type('test@gmail.com');
      cy.get('input[name="password"]').type('test');
      cy.get('.signin-button-button').click();

      // Go to page my adverts
      cy.get('.buttonMyAdvert').click();


      // Mock the backend response for fetching adverts
      cy.intercept('GET', '/advert/client/*', {
        statusCode: 200,
        body: [{
          idAdvert: 1,
          title: 'Advert 1',
          description: 'Description 1',
          price: 450,
          status: 'NORMAL',
          picture: 'path/to/picture1.jpg',
          idClient: 1
        }, {
          idAdvert: 2,
          title: 'Advert 2',
          description: 'Description 2',
          price: 360,
          status: 'NORMAL',
          picture: 'path/to/picture2.jpg',
          idClient: 1
        }]
      }).as('fetchAdverts');
    });
  
    it('displays user adverts correctly', () => {
      // cy.wait('@fetchAdverts');
      cy.get('.AvailableAdvertsTitleText').should('contain', 'Mes annonces');
      cy.get('.Advert').should('have.length', 1);
      cy.get('.AdvertTitleText').first().should('contain', 'Titre de Test');
      cy.get('.AdvertDescriptionTextContent').first().should('contain', 'Description de Test');
      cy.get('.AdvertPriceText').first().should('contain', '666 €');
    });
  
    it('navigates to the edit page when "Modifier" is clicked', () => {
      // cy.wait('@fetchAdverts');
      // Click the "Modifier" button of the first advert
      cy.get('.Advert').first().find('.button-modify-advert').click();
  
      // Verify the navigation has occurred and clicking the button correctly navigates with the advert's state
      cy.url().should('include', '/edit-advert');
    });
  
  });
  