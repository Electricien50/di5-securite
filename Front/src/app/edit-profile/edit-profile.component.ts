import { Component } from '@angular/core';
import { NONE_TYPE } from '@angular/compiler';
import { Client } from '../models/client.models';
import { ActivatedRoute } from '@angular/router';
import { ClientService } from '../services/client.service';
import { SharedDataService } from '../shared-data.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent {
  client : Client = new Client();
  formValid: boolean = true;
  formSubmitted: boolean = false;
  isMailUsed: boolean = false;

  clientForm: FormGroup;

  constructor(private route : ActivatedRoute,
    private clientService: ClientService,
    private sharedDataService: SharedDataService,
    private router: Router,
    private formBuilder: FormBuilder) { 
      this.client = this.sharedDataService.connectedUser;

      this.clientForm = this.formBuilder.group({
        lastName: [this.client.lastName, Validators.required],
        firstName: [this.client.firstName, Validators.required],
        mail: [this.client.mail, Validators.required]
      });

      this.sharedDataService.lastPage = '/edit-profile';
  }

  ngOnInit() {
    // Check if client is logged
    if(!this.sharedDataService.isLogged){
      alert("Vous devez être connecté pour modifier votre profil");
      // Redirect to home page
      this.router.navigate(['/']);
      return;
    }


    this.client = this.sharedDataService.connectedUser;
  }

  getMockClient(): Client {
    const client: Client = {
      idClient: 1,
      firstName: 'Louis',
      lastName: 'BIARD',
      mail: 'louis.biard@etu.univ-tours.fr',
      password: 'Azerty1*'
    };
    return client


  }

  submitForm() {
    // Mettez en œuvre la logique pour soumettre le formulaire ici
    this.formSubmitted = true;
    console.log("Formulaire soumis !");
  }

  onSubmit() {
    this.formSubmitted = true;
    this.formValid = true;

    // Retrieve form values
    this.client.lastName = this.clientForm.get('lastName')?.value;
    this.client.firstName = this.clientForm.get('firstName')?.value;
    this.client.mail = this.clientForm.get('mail')?.value;

    // Check if email respect the pattern
    if(!this.client.mail.match("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")){
      this.formValid = false;
      return;
    }

    if(this.clientForm.valid){
      this.clientService.updateClient(this.client).subscribe(
        {
          next: (response) => {
            if (response) {
              alert("Profil modifié avec succès");
              this.sharedDataService.connectedUser = response;
              this.router.navigate(['/']);
            }
          },
          error: () => {
            this.isMailUsed = true;
          }
        });
    }
    else{
      this.formValid = false;
    };
  }
}
